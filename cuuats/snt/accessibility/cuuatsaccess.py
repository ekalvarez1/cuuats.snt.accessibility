import pandas as pd
import pandana as pdna
import os
import geopandas as geopd
from shapely.geometry import Point, shape
from datetime import timedelta
import fiona
import subprocess
import numpy as np


class CuuatsAccess(object):
    def __init__(self):
        self.bicycle_network = ""
        self.pedestrian_network = ""
        self.bus_network = ""
        self.vehicle_network = ""
        """
        self.pois[destionation name] = [data, nearest_num, method, agg_field]
        """
        self.pois = {}
        self.max_unit = {
            "bicycle_network": 15000,
            "pedestrian_network": 14000,
            "bus_network": 3600,
            "vehicle_network": 52800,
        }  # 10 miles
        self.pois_access_intersection = geopd.GeoDataFrame()
        self.pois_access_segment = geopd.GeoDataFrame()
        self.mode_name = {
            "bicycle": "bicycle",
            "pedestrian": "pedestrian",
            "bus": "bus",
            "vehicle": "vehicle",
        }

    @classmethod
    def convert_geometry(cls, df, column_name="geometry"):
        df["x"] = df[column_name].x
        df["y"] = df[column_name].y
        return df

    @classmethod
    def gpkg_to_mbtiles(cls, gpkg_in_path, output_path, **kwargs):
        cmd = ["ogr2ogr", "-f", "MBTiles", output_path, gpkg_in_path]
        for key, value in kwargs.items():
            cmd.extend(["-dsco", "{}={}".format(key, value)])
        subprocess.check_output(cmd)

    def create_bicycle_network(self, nodes, edges, weight):
        network = pdna.Network(
            node_x=nodes.x,
            node_y=nodes.y,
            edge_from=edges["from"],
            edge_to=edges["to"],
            edge_weights=edges[[weight]],
            twoway=False,
        )
        network.precompute(3000)
        self.bicycle_network = network
        return self

    def create_pedestrian_network(self, nodes, edges, weight):
        network = pdna.Network(
            node_x=nodes.x,
            node_y=nodes.y,
            edge_from=edges["from"],
            edge_to=edges["to"],
            edge_weights=edges[[weight]],
            twoway=False,
        )
        network.precompute(3000)
        self.pedestrian_network = network
        return self

    def create_bus_network(
        self, gtfs_path, date=20181016, time_range=["07:00:00", "09:00:00"]
    ):
        self._process_gtfs(gtfs_path)
        self._filter_trips(date, time_range)
        self._agg_transit_ped()
        return self

    def create_vehicle_network(self, nodes, edges, weight):
        network = pdna.Network(
            node_x=nodes.x,
            node_y=nodes.y,
            edge_from=edges["from"],
            edge_to=edges["to"],
            edge_weights=edges[[weight]],
            twoway=False,
        )
        network.precompute(3000)
        self.vehicle_network = network
        return self

    def save_networks(self, path=None):
        if path:
            os.chdir(path)
        self.bus_network.save_hdf5("bus_network.hdf5")
        self.pedestrian_network.save_hdf5("pedestrian_network.hdf5")
        self.bicycle_network.save_hdf5("bicycle_network.hdf5")
        self.vehicle_network.save_hdf5("vehicle_network.hdf5")
        return self

    def load_networks(self, path=None):
        if path:
            os.chdir(path)
        self.bus_network = pdna.Network.from_hdf5("bus_network.hdf5")
        self.pedestrian_network = pdna.Network.from_hdf5(
            "pedestrian_network.hdf5"
        )
        self.bicycle_network = pdna.Network.from_hdf5("bicycle_network.hdf5")
        self.vehicle_network = pdna.Network.from_hdf5("vehicle_network.hdf5")
        return self

    def set_pois(
        self, data, name, method="nearest", nearest_num=1, agg_field=""
    ):
        self.pois[name] = [data, nearest_num, method, agg_field]
        return self

    def set_max_unit(self, max_unit):
        self.max_unit = max_unit
        return self

    def calculate_accessibility(self):
        bus_network = self.bus_network
        pedestrian_network = self.pedestrian_network
        bicycle_network = self.bicycle_network
        vehicle_network = self.vehicle_network

        networks = {
            "bus_network": bus_network,
            "pedestrian_network": pedestrian_network,
            "bicycle_network": bicycle_network,
            "vehicle_network": vehicle_network,
        }
        geometry = [
            Point(x, y)
            for x, y in zip(bus_network.nodes_df.x, bus_network.nodes_df.y)
        ]
        crs = {"init": "epsg:4326"}
        geodf = geopd.GeoDataFrame(
            bus_network.nodes_df, crs=crs, geometry=geometry
        )
        geodf = geodf.drop(["x", "y"], axis=1)
        for network_name, network in networks.items():
            # prefix = network_name.split('_')[0] + '_'
            prefix = self.mode_name.get(network_name.split("_")[0]) + "_"
            dist = self.max_unit.get(network_name)
            for key, param in self.pois.items():
                data = param[0]
                item = param[1]
                method = param[2]
                agg_field = param[3]
                if method == "nearest":
                    network.set_pois(
                        category=key,
                        maxdist=dist,
                        maxitems=item,
                        x_col=data["x"],
                        y_col=data["y"],
                    )
                    nearest_poi = network.nearest_pois(
                        distance=dist, category=key, num_pois=item
                    )
                    nearest_poi = self._rescale(nearest_poi, reverse=False)
                    nearest_poi.columns = [str(i) for i in range(1, item + 1)]

                    geodf = pd.concat([geodf, nearest_poi[str(item)]], axis=1)
                    geodf = geodf.rename(columns={str(item): prefix + key})
                elif method == "aggregation":
                    data["node_ids"] = network.get_node_ids(data.x, data.y)

                    network.set(
                        data.node_ids, variable=data[agg_field], name=key
                    )
                    df = network.aggregate(
                        dist, type="sum", decay="flat", name=key
                    )
                    df = self._rescale(df, reverse=True)
                    geodf = pd.concat([geodf, df], axis=1)
                    geodf = geodf.rename(columns={0: prefix + key})

        geodf = geodf[geodf["geometry"] != Point(0, 0)]
        self.pois_access_intersection = geodf
        return self

    def join_intersection_segment(self, edges_df):
        pois_access = self.pois_access_intersection.drop(columns=["geometry"])
        f = pd.merge(
            edges_df,
            pois_access,
            left_on="from",
            right_index=True,
            how="inner",
        )
        t = pd.merge(
            edges_df, pois_access, left_on="to", right_index=True, how="inner"
        )
        comb = pd.concat([f, t])
        groupby_col = ["segment_id", "from", "to"]
        groupby_df = geopd.GeoDataFrame(comb.groupby(groupby_col).mean())
        final_df = pd.merge(
            edges_df[["segment_id", "geometry"]],
            groupby_df,
            left_on="segment_id",
            right_on="segment_id",
            how="inner",
        )
        for column in final_df:
            if column != "segment_id" and column != "geometry":
                final_df[column] = final_df[column].astype("int64")
        self.pois_access_segment = final_df
        return self

    def override_score(self, df, transit_mode, score, geometry="segment"):
        if geometry == "segment":
            score = self.pois_access_segment.set_index("segment_id")
            df = df.set_index("segment_id")["functional_classification"]
            merged_df = pd.merge(
                score,
                pd.DataFrame(df),
                left_index=True,
                right_index=True,
                how="left",
            )
            col_to_replace = []
            for mode in transit_mode:
                for key in self.pois.keys():
                    col_to_replace.append(f"{mode}_{key}")
            for index, row in merged_df.iterrows():
                if row["functional_classification"] == "Interstate":
                    row[col_to_replace] = np.nan
                    merged_df.loc[index] = row
            merged_df = merged_df.reset_index().drop(
                columns=["functional_classification"]
            )
            self.pois_access_segment = merged_df

    def calculate_simple_average_destinations(
        self, columns, out_col_name, df_type="segment"
    ):
        for mode in self.mode_name.values():
            col = ["{}_{}".format(mode, c) for c in columns]
            if df_type == "segment":
                self.pois_access_segment = self._average_columns(
                    self.pois_access_segment,
                    col,
                    "{}_{}".format(mode, out_col_name),
                )
            elif df_type == "intersection":
                self.pois_access_intersection = self._average_columns(
                    self.pois_access_intersection,
                    col,
                    "{}_{}".format(mode, out_col_name),
                )

    def calculate_single_accessibility(self, network_name, destination):
        network = getattr(self, network_name)
        network.set_pois(
            category=destination,
            maxdist=self.max_unit.get(network_name),
            maxitems=10,
            x_col=self.pois.get(destination)[0]["x"],
            y_col=self.pois.get(destination)[0]["y"],
        )
        nearest_poi = network.nearest_pois(
            distance=self.max_unit.get(network_name),
            category=destination,
            num_pois=10,
        )
        return nearest_poi

    def remove_columns(
        self, col_to_remove, df_type="segment", all_network=False
    ):
        if not isinstance(col_to_remove, list):
            raise TypeError("col_to_remove must be a list")

        if all_network:
            col_to_remove = [
                x + "_" + y
                for x in self.mode_name.values()
                for y in col_to_remove
            ]

        if df_type == "segment":
            self.pois_access_segment = self.pois_access_segment.drop(
                columns=col_to_remove
            )
        elif df_type == "intersection":
            self.pois_access_intersection = self.pois_access_intersection.drop(
                columns=col_to_remove
            )

    def export(
        self,
        filename="pois_access.gpkg",
        path=None,
        export_feature="segment",
        driver="GPKG",
        layer="segment",
    ):
        if path:
            os.chdir(path)
        if export_feature == "segment":
            self.pois_access_segment.to_file(
                filename, driver=driver, layer=layer
            )
        elif export_feature == "intersection":
            self.pois_access_intersection.to_file(
                filename, driver=driver, layer=layer
            )

    def set_neigborhood(self, path):
        self.neighborhoods = [pol for pol in fiona.open(path, "r")]
        for i in range(0, len(self.neighborhoods)):
            self.neighborhoods[i]["properties"]["score"] = 0
            self.neighborhoods[i]["properties"]["int_count"] = 0
            self.neighborhoods[i]["properties"]["avg"] = 0
        return self

    def set_intersections(self, path):
        self.intersections = [point for point in fiona.open(path, "r")]
        return self

    def calculate_neighborhood_avg(self):
        for i, pt in enumerate(self.intersections):
            point = shape(pt["geometry"])
            # iterate through polygons
            for j, poly in enumerate(self.neighborhoods):
                if point.within(shape(poly["geometry"])):
                    # sum of attributes values
                    self.neighborhoods[j]["properties"]["score"] = (
                        self.neighborhoods[j]["properties"]["score"]
                        + self.intersections[i]["properties"]["avg"]
                    )
                    self.neighborhoods[j]["properties"]["int_count"] = (
                        self.neighborhoods[j]["properties"]["int_count"] + 1
                    )

        for i in range(0, len(self.neighborhoods)):
            print(
                self.neighborhoods[i]["properties"]["score"],
                self.neighborhoods[i]["properties"]["int_count"],
            )
            if self.neighborhoods[i]["properties"]["int_count"] != 0:
                self.neighborhoods[i]["properties"]["avg"] = (
                    self.neighborhoods[i]["properties"]["score"]
                    / self.neighborhoods[i]["properties"]["int_count"]
                )

        return self

    def remove_twoways_segment(self):
        self.pois_access_segment = self.pois_access_segment.groupby(
            ["segment_id"]
        ).first()
        self.pois_access_segment = self.pois_access_segment.reset_index()
        crs = {"init": "epsg:4326"}
        self.pois_access_segment = geopd.GeoDataFrame(
            self.pois_access_segment, crs=crs, geometry="geometry"
        )

    def export_neighborhoods(self, path=None):
        # TODO: Export to GeoJSON
        # json = []
        # for feature in self.neighborhoods:
        #     json.append(feature)
        pass

    def _average_columns(self, df, col, out_col_name):
        df[out_col_name] = df[col].mean(axis="columns")
        return df

    def _find_mean(self, geodf, ignore_col="geometry"):
        geodf["avg"] = geodf.loc[:, geodf.columns != ignore_col].mean(axis=1)
        return geodf

    def _rescale(self, df, reverse=False):
        min = df.min()
        max = df.max()
        if not reverse:
            return (1 - (df - min) / (max - min)) * 100
        else:
            return (df - min) / (max - min) * 100

    def _process_gtfs(self, gtfs_path):
        os.chdir(gtfs_path)
        self.stop_times = self._set_stop_times()
        self.stops = self._set_stops()
        self.trips = self._set_trips()
        self.routes = self._set_routes()
        self.calendar_dates = self._set_calendar_dates()

    def _set_stop_times(self):
        stop_times_col = [
            "trip_id",
            "arrival_time",
            "stop_id",
            "stop_sequence",
        ]
        stop_times = pd.read_csv("stop_times.txt")
        stop_times = stop_times[stop_times_col]
        stop_times.arrival_time = stop_times.arrival_time.apply(
            lambda x: timedelta(hours=int(x[0:2]), minutes=int(x[3:5]))
        )
        return stop_times

    def _set_stops(self):
        stops = pd.read_csv("stops.txt")
        x, y = stops.stop_lon, stops.stop_lat
        stops["node_id"] = self.pedestrian_network.get_node_ids(x, y)
        stops_col = ["stop_id", "node_id", "stop_lon", "stop_lat"]
        return stops[stops_col]

    def _set_trips(self):
        trips_col = ["route_id", "service_id", "trip_id"]
        trips = pd.read_csv("trips.txt")
        return trips[trips_col]

    def _set_calendar_dates(self):
        return pd.read_csv("calendar_dates.txt")

    def _set_routes(self):
        return pd.read_csv("routes.txt")

    def _filter_trips(self, date, time_range=["07:00:00", "09:00:00"]):
        date = int(date)
        start_time = timedelta(
            hours=int(time_range[0][0:2]), minutes=int(time_range[0][3:5])
        )
        end_time = timedelta(
            hours=int(time_range[1][0:2]), minutes=int(time_range[1][3:5])
        )
        stop_times = self.stop_times
        calendar_dates = self.calendar_dates
        trips = self.trips
        cond1 = stop_times["arrival_time"] > start_time
        cond2 = stop_times["arrival_time"] < end_time

        peak_stop_times = stop_times[cond1 & cond2]
        peak_trips = pd.merge(
            peak_stop_times, trips, on="trip_id", how="inner"
        )
        unique_date = calendar_dates.loc[calendar_dates["date"] == date]
        date_peak_trips = pd.merge(
            peak_trips, unique_date, on="service_id", how="inner"
        )
        self.date_peak_trips = date_peak_trips
        self.headway = self._calculate_headway(date_peak_trips)
        return self.date_peak_trips

    def _calculate_headway(self, date_peak_trips):
        first_stop = date_peak_trips.loc[date_peak_trips["stop_sequence"] == 1]
        groupby_service = first_stop.groupby(first_stop["service_id"])
        max_arrival = groupby_service.max().arrival_time
        min_arrival = groupby_service.min().arrival_time
        service_count = groupby_service.count().arrival_time
        time_diff = max_arrival - min_arrival
        headway = {}

        for item in zip(time_diff.iteritems(), service_count):

            service = item[0][0]
            time = item[0][1]
            count = item[1]
            if time == timedelta():
                headway[service] = timedelta(seconds=3600).seconds
            else:
                headway[service] = (time / (count - 1)).seconds
        return headway

    def _agg_transit_ped(
        self, date=20181016, time_range=["07:00:00", "09:00:00"]
    ):

        stop_nodes_peak = pd.merge(
            self.date_peak_trips, self.stops, on="stop_id", how="inner"
        )
        stop_nodes_peak = stop_nodes_peak.sort_values(
            ["trip_id", "stop_sequence"]
        )
        # convert into seconds based on 3 miles per hour
        edges = self.pedestrian_network.edges_df
        edges["weight"] = (edges["ped_weight"] / 5280) / 3 * 60 * 60
        stop = self.pedestrian_network.nodes_df.index.max() + 1
        nodes = self.pedestrian_network.nodes_df
        headway = self.headway
        prev_trip = None
        prev_intersection = None
        prev_stop = None
        prev_time = None
        for row in stop_nodes_peak.iterrows():
            intersection = row[1].node_id
            arrival_time = row[1].arrival_time
            trip = row[1].trip_id
            service_id = row[1].service_id
            same_trip = prev_trip == trip
            if same_trip:
                # getting on the bus
                on_bus = pd.DataFrame(
                    {
                        "from": [prev_intersection],
                        "to": [prev_stop],
                        "weight": [headway.get(service_id, 3600)],
                    },
                    index=[edges.index.max() + 1],
                )
                edges = edges.append(on_bus)

                # getting off the bus
                off_bus = pd.DataFrame(
                    {"from": [stop], "to": [intersection], "weight": [0]},
                    index=[edges.index.max() + 1],
                )
                edges = edges.append(off_bus)

                # transit time between stop
                time_diff = (arrival_time - prev_time).seconds
                edges = edges.append(
                    pd.DataFrame(
                        {
                            "from": [prev_stop],
                            "to": [stop],
                            "weight": [time_diff],
                        },
                        index=[edges.index.max() + 1],
                    )
                )

            # adding stop to transit nodes
            nodes = nodes.append(
                pd.DataFrame({"x": [0], "y": [0]}, index=[stop])
            )

            prev_trip = trip
            prev_intersection = intersection
            prev_stop = stop
            prev_time = arrival_time
            stop = stop + 1

        edges["weight"] = edges["weight"].replace(0, 0.01)

        bus_network = pdna.Network(
            node_x=nodes.x,
            node_y=nodes.y,
            edge_from=edges["from"],
            edge_to=edges["to"],
            edge_weights=edges[["weight"]],
            twoway=False,
        )
        self.bus_network = bus_network
