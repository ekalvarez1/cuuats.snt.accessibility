# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

version = '0.1.0'

setup(name='cuuats.snt.accessibility',
      version=version,
      description='Accessibility calculations for the '
                  'Sustainable Neighborhoods Toolkit',
      long_description='\n'.join([open(f).read() for f in [
          'README.md',
          'CHANGELOG.md'
      ]]),
      long_description_content_type='text/markdown',
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Win32 (MS Windows)',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: BSD License',
          'Topic :: Database',
          'Topic :: Scientific/Engineering :: GIS',
          'Programming Language :: Python :: 3.6',
      ],
      keywords='Sustainable Neigborhood Accessibility',
      author='Edmond Lai',
      author_email='klai@ccrpc.org',
      url='https://ccrpc.org',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['cuuats', 'cuuats.snt'],
      install_requires=[
        'pandas>=0.23.4',
        'numpy>=1.15.1',
        'pandana>=0.4.1',
        'geopandas>=0.4.0',
        'fiona>=1.7.13'
      ])
