import pandas as pd
import sys
from cuuatsaccess import CuuatsAccess
import geopandas as gpd
import os


IGNORE_SEGMENTS = [15, 15097, 15141]
IGNORE_INTERSECTIONS = [10498, 11157, 11544, 11541, 9899, 9898]


def get_projects():
    project_path = "/mnt/git/snt-scenario-editing/src/bike_projects.gpkg"
    layer = "BikeProjects_finaledits"
    gdf = gpd.read_file(project_path, driver="GPKG", layer=layer)
    gdf = gdf[["data-1567615426275_name", "geometry"]]
    gdf = gdf.rename(columns={"data-1567615426275_name": "name"})
    return gdf


def remove_interstate(edges):
    return edges[edges.functional_classification != "Interstate"]


def main(gpkg_path, gtfs_path, out_path, include_projects="False"):
    # Create edges and nodes from geopackage
    edges = gpd.GeoDataFrame.from_file(
        gpkg_path, driver="GPKG", layer="result"
    )
    edges_clone = edges.copy(deep=True)
    # Remove problematic segments
    for seg in IGNORE_SEGMENTS:
        edges = edges[edges["segment_id"] != seg]

    edges["bike_weight"] = (
        edges["geometry"].to_crs({"init": "epsg:3435"}).length
        * edges["blts_score"]
    )
    edges["ped_weight"] = (
        edges["geometry"].to_crs({"init": "epsg:3435"}).length
        * edges["plts_score"]
    )
    edges["vehicle_weight"] = (
        edges["geometry"].to_crs({"init": "epsg:3435"}).length
        * edges["alts_score"]
    )
    # Create two ways network
    edges_two_way = edges
    edges_two_way = edges_two_way.rename(
        index=str, columns={"start_intersection_id": "from"}
    )
    edges_two_way = edges_two_way.rename(
        index=str, columns={"end_intersection_id": "to"}
    )
    edges = edges.rename(index=str, columns={"start_intersection_id": "to"})
    edges = edges.rename(index=str, columns={"end_intersection_id": "from"})
    edges = edges.append(edges_two_way)
    edges = edges.rename(index=str, columns={"segment_id": "id"})
    edges["bike_weight"] = pd.to_numeric(edges["bike_weight"])
    edges["ped_weight"] = pd.to_numeric(edges["ped_weight"])
    edges["vehicle_weight"] = pd.to_numeric(edges["vehicle_weight"])
    edges.dropna(inplace=True, subset=["from", "to"])
    edges = edges.reset_index()
    edges_without_interstate = remove_interstate(edges)

    # Getting the intersection nodes
    nodes = gpd.GeoDataFrame.from_file(gpkg_path, driver="GPKG", layer="nodes")
    # Remove problematic intersections
    for intersection in IGNORE_INTERSECTIONS:
        nodes = nodes[nodes["intersection_id"] != intersection]
    nodes = nodes[["x", "y", "intersection_id"]]
    nodes = nodes.rename(columns={"intersection_id": "id"})
    nodes = nodes.set_index("id")

    # Getting the segment for joining
    edges_geom = gpd.GeoDataFrame.from_file(
        gpkg_path, driver="GPKG", layer="result"
    )
    edges_geom = edges_geom.rename(
        index=str,
        columns={"start_intersection_id": "from", "end_intersection_id": "to"},
    )
    edges_geom.dropna(inplace=True, subset=["from", "to"])
    edges_geom = edges_geom[["geometry", "from", "to", "segment_id"]]

    # setting up destinations
    destination = gpd.GeoDataFrame.from_file(
        gpkg_path, driver="GPKG", layer="destination"
    )
    destination = CuuatsAccess.convert_geometry(destination)

    # grocery stores
    grocery = destination[destination["type"] == "grocery"]
    art = destination[destination["type"] == "art"]
    service = destination[destination["type"] == "service"]
    preschool = destination[
        (destination["type"] == "school")
        & (destination["category"] == "preschool")
    ]
    high_school = destination[
        (destination["type"] == "school") & (destination["category"] == "high")
    ]
    other_school = destination[
        (destination["type"] == "school")
        & (destination["category"] == "other")
    ]
    elementary_school = destination[
        (destination["type"] == "school")
        & (destination["category"] == "elementary")
    ]  # noqa
    middle_school = destination[
        (destination["type"] == "school")
        & (destination["category"] == "middle")
    ]  # noqa
    retail = destination[destination["type"] == "retail"]
    restaurant = destination[destination["type"] == "restaurant"]
    public_building = destination[
        (destination["type"] == "public")
        & (destination["category"] == "public")
    ]
    emergency = destination[
        (destination["type"] == "public")
        & (destination["category"] == "emergency")
    ]
    recreation = destination[
        (destination["type"] == "public")
        & (destination["category"] == "recreation")
    ]
    post_office = destination[
        (destination["type"] == "public") & (destination["category"] == "post")
    ]
    library = destination[
        (destination["type"] == "public")
        & (destination["category"] == "library")
    ]
    park = destination[destination["type"] == "park"]
    pharmacy = destination[
        (destination["type"] == "health")
        & (destination["category"] == "pharmacy")
    ]
    medical_facility = destination[
        (destination["type"] == "health")
        & (destination["category"] == "medical")
    ]
    job = destination[destination["type"] == "job"]

    cuuatsaccess = CuuatsAccess()

    print("creating bicycle network...")
    cuuatsaccess.create_bicycle_network(
        nodes, edges_without_interstate, "bike_weight"
    )
    print("creating pedestrian network...")
    cuuatsaccess.create_pedestrian_network(
        nodes, edges_without_interstate, "ped_weight"
    )

    print("creating bus network...")
    cuuatsaccess.create_bus_network(
        gtfs_path, date=20181016, time_range=["07:00:00", "09:00:00"]
    )
    print("creating vehicle network...")
    cuuatsaccess.create_vehicle_network(nodes, edges, "vehicle_weight")

    # cuuatsaccess.save_networks()
    # os.chdir('/mnt/data')
    # cuuatsaccess.load_networks('gtfs_data')

    print("creating pois...")
    cuuatsaccess.set_pois(grocery, "grocery", method="nearest", nearest_num=1)
    cuuatsaccess.set_pois(
        medical_facility, "health_medical", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        pharmacy, "health_pharmacy", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(art, "art", method="nearest", nearest_num=1)
    cuuatsaccess.set_pois(
        recreation, "public_recreation", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        public_building, "public_public", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        post_office, "public_post", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        library, "public_library", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        emergency, "public_emergency", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        restaurant, "restaurant", method="nearest", nearest_num=5
    )
    cuuatsaccess.set_pois(job, "job", method="aggregation", agg_field="weight")
    cuuatsaccess.set_pois(
        elementary_school, "school_elementary", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        middle_school, "school_middle", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        high_school, "school_high", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        preschool, "school_preschool", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(
        other_school, "school_other", method="nearest", nearest_num=1
    )
    cuuatsaccess.set_pois(retail, "retail", method="nearest", nearest_num=5)
    cuuatsaccess.set_pois(service, "service", method="nearest", nearest_num=5)
    cuuatsaccess.set_pois(park, "park", method="nearest", nearest_num=1)
    cuuatsaccess.set_max_unit(
        {
            "bicycle_network": 21120,
            "pedestrian_network": 15840,
            "bus_network": 3600,
            "vehicle_network": 105600,
        }
    )
    print("calculating accessibility...")
    cuuatsaccess.calculate_accessibility()
    cuuatsaccess.join_intersection_segment(edges_geom)
    cuuatsaccess.override_score(
        edges_clone[edges_clone["functional_classification"] == "Interstate"],
        ["pedestrian", "bicycle", "bus"],
        0,
    )

    health_col_to_avg = ["health_medical", "health_pharmacy"]
    out_col_name = "health"
    cuuatsaccess.calculate_simple_average_destinations(
        health_col_to_avg, out_col_name
    )
    cuuatsaccess.calculate_simple_average_destinations(
        health_col_to_avg, out_col_name, df_type="intersection"
    )

    public_col_to_avg = [
        "public_recreation",
        "public_public",
        "public_post",
        "public_library",
        "public_emergency",
    ]
    out_col_name = "public"
    cuuatsaccess.calculate_simple_average_destinations(
        public_col_to_avg, out_col_name
    )
    cuuatsaccess.calculate_simple_average_destinations(
        public_col_to_avg, out_col_name, df_type="intersection"
    )
    school_col_to_avg = [
        "school_elementary",
        "school_middle",
        "school_high",
        "school_preschool",
        "school_other",
    ]
    out_col_name = "school"
    cuuatsaccess.calculate_simple_average_destinations(
        school_col_to_avg, out_col_name
    )
    cuuatsaccess.calculate_simple_average_destinations(
        school_col_to_avg, out_col_name, df_type="intersection"
    )
    networks = [
        "bicycle_network",
        "pedestrian_network",
        "bus_network",
        "vehicle_network",
    ]
    pois = [
        "grocery",
        "health_medical",
        "health_pharmacy",
        "art",
        "public_recreation",
        "public_public",
        "public_post",
        "public_library",
        "public_emergency",
        "restaurant",
        "school_elementary",
        "school_middle",
        "school_high",
        "school_preschool",
        "school_other",
        "retail",
        "service",
        "park",
    ]

    # for network in networks:
    #     print(network)
    #     for poi in pois:
    #         print(poi)
    #         df = cuuatsaccess.calculate_single_accessibility(
    #                     network_name=network,
    #                     destination=poi
    #         )
    #         file_name = network.split('_')[0] + '_' + poi
    #         df.to_csv(os.path.join('/home/edmondlai/Desktop/lts_result/',
    #                                '{}.csv'.format(file_name)))
    # rest = cuuatsaccess.calculate_single_accessibility(
    #                                         network_name='bicycle_network',
    #                                         destination='restaurant')
    # service = cuuatsaccess.calculate_single_accessibility(
    #                                         network_name='bicycle_network',
    #                                         destination='service')
    # retail = cuuatsaccess.calculate_single_accessibility(
    #                                         network_name='bicycle_network',
    #                                         destination='retail')
    #
    # rest.to_csv(os.path.join('/home/edmondlai/Desktop', 'rest.csv'))
    # service.to_csv(os.path.join('/home/edmondlai/Desktop', 'service.csv'))
    # retail.to_csv(os.path.join('/home/edmondlai/Desktop', 'retail.csv'))
    #
    # cuuatsaccess.export('pois_access_intersection.geojson',
    #                     export_feature='intersection')

    col_to_remove = []
    col_to_remove.extend(health_col_to_avg)
    col_to_remove.extend(public_col_to_avg)
    col_to_remove.extend(school_col_to_avg)

    # join name, cross_name_start and cross_name_end
    print("join name, cross_name_start and cross_name_end")
    edges_name = edges[["id", "name", "cross_name_start", "cross_name_end"]]
    cuuatsaccess.pois_access_segment = cuuatsaccess.pois_access_segment.merge(
        edges_name, left_on="segment_id", right_on="id", how="inner"
    )

    cuuatsaccess.remove_twoways_segment()

    cuuatsaccess.remove_columns(col_to_remove, all_network=True)
    cuuatsaccess.remove_columns(
        col_to_remove, all_network=True, df_type="intersection"
    )

    os.chdir(out_path)
    cuuatsaccess.export(
        "access_data_segment_id.gpkg", export_feature="segment"
    )
    cuuatsaccess.export(
        "access_data_segment_id.gpkg",
        export_feature="intersection",
        layer="intersection",
    )
    cuuatsaccess.remove_columns(["id"], all_network=False)
    cuuatsaccess.export("access_data.gpkg", export_feature="segment")
    cuuatsaccess.export(
        "access_data.gpkg", export_feature="intersection", layer="intersection"
    )

    print("destination to file")
    cuuatsaccess.export("pois_access_segment.gpkg", export_feature="segment")
    destination = destination.drop(columns=["weight", "x", "y"])
    destination.to_file(
        "pois_access_segment.gpkg", driver="GPKG", layer="destination"
    )

    if include_projects == "True":
        projects = get_projects()
        projects.to_file(
            "pois_access_segment.gpkg", layer="project", driver="GPKG"
        )

        config = """
                {
                  "destination": {
                    "description": "Destinations used to calculate accessibility",
                    "minzoom": 12,
                    "maxzoom": 14
                  },
                  "segment": {
                    "description": "Street segments with accessibility scores",
                    "minzoom": 0,
                    "maxzoom": 14
                  },
                  "project": {
                    "description": "future scenario projects",
                    "minzoom": 0,
                    "maxzoom": 14
                  },
                }
                """
    else:
        config = """
            {
              "destination": {
                "description": "Destinations used to calculate accessibility",
                "minzoom": 12,
                "maxzoom": 14
              },
              "segment": {
                "description": "Street segments with accessibility scores",
                "minzoom": 0,
                "maxzoom": 14
              }
            }
            """
    print("to mbtiles...")
    CuuatsAccess.gpkg_to_mbtiles(
        gpkg_in_path="pois_access_segment.gpkg",
        output_path="pois_access.mbtiles",
        NAME="mbtiles",
        DESCRIPTION="Accessibility scores from the Sustainable Neighborhoods analysis of Champaign County, Illinois",
        MINZOOM=0,
        MAXZOOM=14,
        CONF=config,
    )
    print("Finished!")


if __name__ == "__main__":
    # argv[1] gpkg path, argv[2] gtfs folder path, outpath
    main(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
