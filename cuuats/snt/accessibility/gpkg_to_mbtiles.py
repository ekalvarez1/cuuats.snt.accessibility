from cuuatsaccess import CuuatsAccess

ca = CuuatsAccess()
config = \
    """
    {
      "destination": {
        "description": "Destinations used to calculate accessibility",
        "minzoom": 12,
        "maxzoom": 14
      },
      "segment": {
        "description": "Street segments with accessibility scores",
        "minzoom": 0,
        "maxzoom": 14
      }
    }
    """
ca.gpkg_to_mbtiles(gpkg_in_path='/home/edmondlai/Desktop/lts_result.gpkg',
                   output_path='/home/edmondlai/Desktop/test_tile.mbtiles',
                   NAME='mbtiles',
                   DESCRIPTION='Accessibility scores from the Sustainable Neighborhoods analysis of Champaign County, Illinois',
                   MINZOOM=0,
                   MAXZOOM=14,
                   CONF=config
                   )
