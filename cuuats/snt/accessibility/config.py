NODES_SQL = """
SELECT intersection_id AS id,
    ST_X(ST_Transform(geom, 4326)) AS x,
    ST_Y(ST_Transform(geom, 4326)) AS y
FROM street.intersection
WHERE is_node = 'Yes'
"""

EDGES_SQL = """
SELECT s.segment_id as id,
    s.start_intersection_id AS from,
    s.end_intersection_id AS to,
    (ST_Length(geom) * l.blts)::numeric AS bike_weight,
    (ST_Length(geom) * l.plts)::numeric AS ped_weight
FROM street.segment s
    JOIN street.lts_score l
        ON s.segment_id = l.segment_id
WHERE s.start_intersection_id IS DISTINCT FROM NULL AND
    s.end_intersection_id IS DISTINCT FROM NULL
UNION
SELECT s.segment_id as id,
    s.end_intersection_id AS from,
    s.start_intersection_id AS to,
    (ST_Length(geom) * l.blts)::numeric AS bike_weight,
    (ST_Length(geom) * l.plts)::numeric AS ped_weight
FROM street.segment s
    JOIN street.lts_score l
        ON s.segment_id = l.segment_id
WHERE s.start_intersection_id IS DISTINCT FROM NULL AND
    s.end_intersection_id IS DISTINCT FROM NULL
"""

EDGES_GEOM_SQL = """
SELECT segment_id,
    start_intersection_id AS from,
    end_intersection_id AS to,
    ST_Transform(geom, 4326) as geom
FROM street.segment
WHERE start_intersection_id IS DISTINCT FROM NULL AND
    end_intersection_id IS DISTINCT FROM NULL
"""

PARK_SQL = """
SELECT id,
    ST_X(ST_Centroid(ST_Transform(geom, 4326))) AS x,
    ST_Y(ST_Centroid(ST_Transform(geom, 4326))) AS y
FROM land_use.open_space
WHERE classification = 'Signature' OR
    classification = 'Community' OR
    classification = 'Univeristy Greenway' OR
    space_type = 'Foreset Preserve'
"""
