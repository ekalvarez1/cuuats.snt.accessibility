import os
import pandas as pd
import matplotlib.pyplot as plt
from utils import HOME_PATH


def read_df(dir_path, file_name):
    df = pd.read_csv(os.path.join(dir_path, file_name), index_col='id')
    return df


def export_pct_change_plot(df, title):
    plot_df = df.replace(0, 1)
    plot_df = plot_df.pct_change(axis='columns').mean(axis=0)
    plt.figure()
    plt.title(title)
    plot_df.plot()
    ticks = [int(x) for x in df.columns.tolist()]
    plt.xticks(ticks, df.columns.tolist())
    plt.savefig(os.path.join(HOME_PATH, '{}.png'.format(title)))
    plt.close()


def main():
    rest = read_df(HOME_PATH, 'rest.csv')
    service = read_df(HOME_PATH, 'service.csv')
    retail = read_df(HOME_PATH, 'retail.csv')

    export_pct_change_plot(rest, 'restaurant pct change')
    export_pct_change_plot(service, 'service pct change')
    export_pct_change_plot(retail, 'retail pct change')


if __name__ == '__main__':
    main()
