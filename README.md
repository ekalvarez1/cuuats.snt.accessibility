# cuuats-snt-accessibility
Based on the network analysis tool [Pandana](https://github.com/UDST/pandana).  This accessibility tool is part of the Sustainable Neighborhood Toolkit that calculate destination accessibility based on LTS (Level of Traffic Stress).

## Dependency
Dependency includes and should be installed with setup.py
  - pandas
  - numpy
  - pandana
  - geopandas
  - fiona

## Data Preparation

### Nodes and Edges
Pandana requires network in nodes and edges to create a network for analysis.

The format for Nodes and Edges is as followed, they are in pandas.DataFrame:

| id (int) | x (float) | y (float) |
| --- | --- | --- |
| 11222 | "-88.5011433889141" | "40.1929076445994" |
| 11223 | "-88.4945263479992" | "40.2075186070895" |

The coordinate is transformed into WSG84(epsg: 4326) for the ease of combining with transit data and web mapping.

| id | from | to | bike_weight | ped_weight | auto_weight |
| --- | --- | --- | --- | --- | --- |
| 1 | 2950 | 2930 | 370.0266 | 555.0399 | 555.0399 |
| 1 | 2930 | 2930 | 370.0266 | 555.0399 | 555.0399 |
| 2 | 250 | 249 | 114.4265 | 457.6856 | 457.6856 |
| 2 | 249 | 250 | 114.4265 | 457.6856 | 457.6856 |

The calculate the weight for each mode of transportation, it is currently using this formula:
  - **length of segment * lts score**


### GTFS data
CUUATS developed the tool using Champaign-Urbana MTD GTFS data can be download [Here](https://developer.cumtd.com/|here).  The downloaded files can be in the repository for processing.  

Example: cuuats/snt/accessibility/gtfs_data/

cuuats.snt.accessibility library will handle the processing for the GTFS data.

### Destinations
| id | type | category | name | weight | x | y |
| --- | --- | --- | --- | --- | --- | --- | --- |
| 1 | public | recreation | My park | | -88.2347 | 40.1166 |
| 2 | school | preschool | School name| | -88.2395 | 40.1102 |

The type is the primary grouping for destination type.

#### Destinations Calculation
There are ten destinations categories, each are calculated slightly differently.  This section explain how each of the categories are calculated.


##### Grocery Stores
The score for grocery store is calculated based on **closest** destination.

##### Health Facilities
Health facilities are broken down into 2 subtypes: `medical facilities` and `pharmacies`.  The health facilities score is based on the average of the `medical facilities` and `pharmacies` to the **closet** destination.

##### Arts and Entertainment
The score for arts and entertainment is calculated based on **closest** destination.

##### Public Facilities
Public Facilities are broken down into 5 subtypes: `recreation`, ` public facilities`, `post office`, `library`, and `emergency services`.  The public facilties score is based on the average of the **closest** destination to the above subtypes.

##### Restaurants
The score for restaurant is calculated based on **fifth** closet destination.

##### Job
The job score is calculated differently than other destination types.  Based on the threshold set for each mode of transportation, all the jobs accessibility within the threshold are sum together and the score is based on the **summation** of jobs accessible.

##### School
Schools are broken down into 5 subtypes: `elementary`, `middle`, `high`, `presechool`, and `other`.  The school score is based on the average of the **closest** destination to the above subtypes.

##### Retail
The score for retail is calculated based on **fifth** closet destination.

##### service
The score for service is calculated based on **fifth** closet destination.

##### Park
The score for service is calculated based on **closet** destination.

## Network Creation
First, create the cuuatsaccess object.

```python
from cuuats.snt.accessiblity import CuuatsAccess
cuuatsaccess = CuuatsAccess()
```

### Bicycle, Pedestrian, Vehicle
This object contains the methods needed to create the different networks, set point of interests and calculate accessibility.

To create bicycle, pedestrian, and vehicle network, use the following code.  The methods take the nodes and edges pandas dataframe object prepare earlier.  The third argument is the column name of the weight in the edges dataframe.

```python
cuuatsaccess.create_bicycle_network(nodes, edges, 'bike_weight')
cuuatsaccess.create_pedestrian_network(nodes, edges, 'ped_weight')
cuuatsaccess.create_vehicle_network(nodes, edges, 'vehicle_weight')
```

The previous three methods take the following argument.
- nodes: `pandas.DataFrame` object
- edges: `pandas.DataFrame` object
- weight: `string` of the column that represents the weight in the edges dataframe

### Transit
The transit network is different than the previous three networks because it is constructed from the GTFS data earlier.  CuuatsAccess class will parse the GTFS data based on the date and time provided to construct a network.  In order for the bus network to be created successfully, the pedestrian network must be provided.  CuuatsAccess aggregate the bus network with the pedestrian to simulate the real life situation of taking transit and then traverse to the final destination as a pedestrian. 

```python
cuuatsaccess.create_bus_network(gtfs_path, date=20181016, time_range=['07:00:00', '09:00:00']))
```

The `create_bus_network` take the following arguments:
- gtfs_path: string representation of the directory for the GTFS data
- date: int value representation the date of which GTFS data will be used
- time_range: list of string representation of the time range of which network will be constructed


## Saving and Loading Network
The network creation process is time consuming.  CuuatsAccess class allows the user to save the load existing network from file to reduce time. The networks are saves as `bicycle_network.hdf5`, `bus_network.hdf5`, `pedestrian_network.hdf5`, `vehicle_network.hdf5`

```python
# Save network
cuuatsaccess.save_networks(dir_path)
# Load network
cuuatsaccess.load_networks(dir_path)
```

- dir_path: `string` argument for the path to a directory

## Setting Point of Interests
CuuatsAccess class calculate the accessibility score for all the destinations type.  In order to achieve this, first the point of interests need to be added to the cuuatsaccess object for accessibility calculation.

```python
cuuatsaccess.set_pois(destination, 'destination', method='nearest', nearest_num=1)
```

The `set_pois` take the following arguments.  The `destination` dataFrame prepared previously need to be filtered down to the individual destination type.  For example, retails can be filtered down by using the following code

```python
retails = destination[destination["type"] == "retails"]
```

- data: `pandas.DataFrame` object of the point of interest
- name: `string`, an alias for the point of interest
- method: `string`: `nearest` or `aggregation`
  - `nearest` evaluates the n closet point of interest based on `nearest_num` and return the aggregated unit for distance and LTS score
  - `aggregation` returns how many units is accessible within a certain threshold. For example: point of interest with job numbers. CuuatsAccess will return the number of jobs within the threshold set by aggregating teh `agg_field` in the destination dataframe.
- nearest_num: `int`, the n nearest pois being returned, setting it to 1 returns the closet destination, and setting it to 3 return the third closet destination.
- agg_field: `string`, the column that will be used for aggregation, example: 'emp_num'

Example
```python
cuuatsaccess.set_pois(retails, 'retails', method='nearest', nearest_num=3)
cuuatsaccess.set_pois(restaurants, 'restaurants', method='nearest', neartest_num=1)
cuuatsaccess.set_pois(jobs, 'jobs', method='aggregation', agg_field='emp_num')
```

## Setting Threshold
The network threshold is used is set the maximum cutoff for each mode.

```python
cuuatsaccess.set_max_unit({'bicycle_network': 21120,
                           'pedestrian_network': 15840,
                           'bus_network': 3600,
                           'vehicle_network': 105600})
```

set_max_unit: `dictionary`, it takes a dictionary with network name as the key, the value is the threshold for that network

### Threshold explanation
The threshold is an aggregation of distance, time, and the Level of Traffic Stress Score

#### Biycle
The bicycle threshold is based on these assumption:

- 8 mi/hour biking speed
- Maximum biking of 30 minutes on a LTS 1 segment

**8 mi/hr x 5280 feet x .5 hr x 1 lts = 21120**

#### Pedestrian
The pedestrian threshold is based on:

- 3mi/hour walking speed
- Maximum walking of 30 minutes on a LTS 2 segment

**3 mi/hr x x 5280 feet x .5 hr x 2 lts = 15840**

#### Vehicle
The vehicle threshold is based on:

- Willing to travel 10 miles on a LTS 2 segment

**10 miles * 5280 feet * 2 lts = 105600**

#### Transit
Transit threshold is different than the previous mode given that distance and Level of Traffic Stress is not a factor.  The threshold is solely depends on the time traveled and it is set to be 1 hour.

**1 hour = 3600 seconds**

## Calculate Accessibility
After setting all the above parameter, call the calculate_accessibility method 

```python
cuuatsaccess.calculate_accessibility()
```

This method will loop through each network and each destination type to create a geopandas.GeoDataFrame that contains accessibility information as a property in cuuatsaccess. The resulting geodataframe has the score based on the intersection. 

## Additional Processing Methods
The CuuatsAccess class has additional methods that helps with post processing of the data into another format and they are explain in the following.

### Joining Intersections to Segment
Accessibility score is calculated based on intersection.  For display or using the score for further analysis, it can be aggregated to the street segment using `join_intersection_segment()`.

```python
cuuatsaccess.join_intersection_segment(edges_geom)
```

- edges_geom: geopandas.GeoDataFrame, from and to contain the `id` for the intersection

| segment_id | from | to | geom | 
| --- | --- | --- | --- |
| 1 | 100 | 101 | LineString | 

The method will join the two interesection at the end of each segment and assign the mean scores of the intersections to the segment.

### Calculating Mean Value for Subtype
The `calculate_simple_average_destinations()` method can take mutliple columns and create an average scores. It is helpful if there are multiple destinations subtype for a single category.  See the following example.  The takes multiple health facilities type and create a single score for the health facilities category.

```python
col_to_avg = ['health_facilities_medical_facilities',
            'health_facilities_pharmacies']
out_col_name = 'health_facilities'
cuuatsaccess.calculate_simple_average_destinations(col_to_avg, out_col_name)
```

- col_to_avg: `list` of `string`, name of the columns to be averaged without the mode prefix.
- out_col_name: `string`, the name for the output column.
- df_type (optional): `segment` or `intersection`, which DataFrame to perform the averaging function.  When choosing segment, it must first run the `join_intersection_segment()` method. 

### Remove Two Ways Segment
This method remove segment that has the same `segment_id`.

**TODO**: Supply a different id to the method.

```python
cuuatsaccess.remove_twoways_segment()
```

## Export Results
The result can be export to any file type that is supported by GeoPandas which usees Fiona using the `export` method.  

```python
cuuatsaccess.export('access_data.gpgk', 'path/to/file/', export_feature='segment', layer='segment')
```

- filename: `string`, name of the file
- path: `string`, path to where the file should be exported
- export_feature: `segment` or `intersection`, choose which should be exported
- driver: `string`, driver name that is supported by Fiona
- layer: `string`, output layer name if supported by the driver.